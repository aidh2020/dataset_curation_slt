import os
from moviepy.editor import VideoFileClip
import glob
import csv

def with_moviepy(filename):
    clip = VideoFileClip(filename)
    duration = clip.duration
    # fps = clip.fps
    # width, height = clip.size
    return duration

def main():

    received_zip = glob.glob("C:\\Users\\ezln9\\Desktop\\gb\\*.zip")
    print(received_zip)
    video_details = []

    for i in received_zip:
        # Name of the folder with videos
        print( i.split(".zip"))
        file_path = i.split(".zip")[0] + "\\"
        # List of available files
        files = glob.glob(file_path + "*.MOV")

        # Display active folder
        print(files)

        for file in files:
            # Name of the video
            print(file_path)

            # print(file)
            # print(file.split(file_path)[1].split("."))
            video_name = file.split(file_path)[1].split(" ")[0]
            # Get video size
            video_size = os.path.getsize(file)
            # Get video duration in bytes
            video_duration = with_moviepy(file)

            video_details.append([video_name, round(video_size/1024/1024, 2) , video_duration])

        print(video_details)


    with open("out.csv", "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerows(video_details)



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

