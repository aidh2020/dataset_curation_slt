# Curated Datasets for Greek Sign Language Translation

The largest volume of signed texts by natural signers that have ever been created in Greece. 
The lexical, morphological, syntactic and pragmatic content of the data is vast, quantitatively and qualitatively. 

|            | Elementary | News       |
|------------|:-----------|:-----------|
| Signers    | 7          | 4          |
| Duration   | 71H        | 25H        |
| Frames     | 6.391.500  | 2.669.647  |
| Sentences  | 28.357     | 2.043      |
| Vocabulary | 42.363     | 6.741      |
| Singletons | 24.864     | 2.538      |
| Resolution | 1280 x 720 |1920 × 1080 |
| FPS        |  25        |  29.97     |

The available datasets can be found at:

https://alucutac-my.sharepoint.com/:f:/g/personal/c_partaourides_cut_ac_cy/EnjA9l-JBSVMtfD7G0avJTIBuBP227xQaVfhyFC0TUd_9Q?e=2PYDXF

## Elementary Dataset

Entails the course material of the first years of elementary school in Greece. 
It includes 28.357 signed phrases that are present in the 33 issues of 13 distinct textbooks of the A, B and C years of Primary school. 

The Elementary Dataset consists of the following courses:
* Greek Language (1st, 2nd and 3rd year)
* Mathematics (1st, 2nd and 3rd year)
* Anthology of Greek Literacy (1st, 2nd, 3rd and 4th year)
* Environmental Studies (1st, 2nd and 3rd year)
* History (3rd year)
* Religious Study (3rd year)

![ALT](/images/elementary_example.jpg)

## News dataset

The News Dataset consists of 2.043 signed phrases of crime-related news stories broadcasted in Greece.

![ALT](/images/news_example.jpg)

## Acknowledgements

This dataset is supported by European Union’s Horizon 2020 research and innovation programme under grant agreement No 872139, project aiD (artificial intelligence for the Deaf).
