import numpy as np
import cv2
import os

size = (227, 227)


def break_to_images(size, filepath):
    # Scan all folder files
    # change directory to filepath without file
    vidcap = cv2.VideoCapture(filepath + '.flv')

    if not os.path.isdir('../' + filepath):
        os.mkdir('../' + filepath)

    success, image = vidcap.read()
    count = 0
    frames = []
    print('before', success)
    while success:
        count += 1
        success, image = vidcap.read()

        if image is None:
            print('ok')
            break
            #### Frames -- > Resized
        image_crop = image[:, 280:-280]
        image_resized = cv2.resize(image_crop, size)
        #### Save Resized Frames
        cv2.imwrite('../' + filepath + '/images' + str(count).zfill(4) + '.png', image_resized)

        frames.append(image_resized)

    image_tensor = np.array(frames)
    print('Shape of array:', image_tensor.shape)
    return image_tensor, frames


def reconstruct_to_videos(image_tensor, frames, size, filepath, fps=25, seconds=10):
    per_file = fps * seconds
    segment_id = 0
    video_status = True
    while video_status == True:

        if segment_id == 100: break
        print('Video segment:', segment_id)
        out = cv2.VideoWriter(filepath + '_' + str(segment_id) + '.mp4', cv2.VideoWriter_fourcc(*'mp4v'), fps, size)
        if segment_id * per_file == image_tensor.shape[0]: break

        try:
            for i in range(segment_id * per_file, segment_id * per_file + per_file):
                out.write(frames[i])
        except IndexError:
            video_status = False
            for i in range(segment_id * per_file, image_tensor.shape[0]):
                out.write(frames[i])
        out.release()
        segment_id += 1

    print('Done!')


filepath = 'videos'
os.chdir(filepath)
list_of_files = [file.split('.')[0] for file in os.listdir() if
                 (file.split('.')[1] == 'MOV' or file.split('.')[1] == 'flv')]
print(list_of_files)

for file in list_of_files:
    (image_tensor, frames) = break_to_images(size, file)

    # reconstruct_to_videos(image_tensor, frames, size, file, fps=25, seconds=10)

exit()
