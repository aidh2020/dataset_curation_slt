import pandas as pd
import os
import pip
import xlrd
import csv
import numpy as np
data = pd.ExcelFile("C:\\Users\\ezln9\\Desktop\\all_elementary.xlsx")
dataset = "C:\\Users\\ezln9\\Desktop\\all_elementary.xlsx"
print(data.sheet_names)
df = data.parse('Φύλλο1')
print(df.columns)
print(df.head(3))
print(df)

# 1. Go to folder (cd ....)
# 2. Run command (bin/e)
# 3. Force stop ( ctrl + C, x )
#calculating mean,max,min,sum size and duration
print(df.iloc[:,8])
d_mean = np.mean(df.iloc[:,8])
d_max = np.max(df.iloc[:,8])
d_min = np.min(df.iloc[:,8])
d_sum = np.sum(df.iloc[:,8])
print([d_mean],[d_max],[d_min],[d_sum])
s_mean = np.mean(df.iloc[:,7])
s_max = np.max(df.iloc[:,7])
s_min = np.min(df.iloc[:,7])
s_sum = np.sum(df.iloc[:,7])
print([s_mean],[s_max],[s_min],[s_sum])

#how many words appear in every sentence
df['words per row'] = df['Transcription'].str.split().str.len()
print(df['words per row'])


#total words of the text
total_words = np.sum(df['words per row'])
print([total_words])


#words' freequency and 50 most common words
word_frequency = df['Transcription'].str.lower().str.split(expand=True).stack().value_counts()
print(word_frequency)
word_frequency.to_csv("out1.csv")
common_words = df['Transcription'].str.lower().str.split(expand=True).stack().value_counts()[:50]
print(common_words)
print(np.sum(common_words))

print(word_frequency.keys())
print(word_frequency.values)

# find singletons(words that appear only once in the sentences)
for word, count in zip(word_frequency.keys(), word_frequency.values):
    if count == 1:
     print(word)
    else:
        pass





